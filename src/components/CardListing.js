import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";

export default class CardListing extends Component {


    render() {
        const { name, imgUri, address, standardPriceMondayToThursday, numberOfMaximumGuest, bedroomCount, onPress } = this.props;

        return (
            <View style={styles.viewItem}>
                <View style={styles.viewTouchSub}>

                    <TouchableOpacity onPress={onPress}>
                        <View style={styles.viewTouchSubTop}>

                            <Image style={styles.imgIconItem} source={{ uri: "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/minimal-bedroom-small-spaces-1551546274.jpg" }} />

                            <View style={styles.viewSubTopInfo}>
                                <Text numberOfLines={1} style={{ width: "100%", fontSize: 19, fontWeight: "700", overflow: 'hidden' }}>
                                    {name}
                                </Text>
                                <Text style={{ color: '#9ea0a4',width:"100%" ,marginVertical: 5 }}>
                                    {address}
                                </Text>
                                <Text style={{ marginVertical: 10 }}>
                                    <Text style={{ fontSize: 14, fontWeight: '700', color: '#ff8519' }}>VND {standardPriceMondayToThursday} </Text>
                                    <Text style={{ color: '#9ea0a4' }}>/night</Text>
                                </Text>
                            </View>

                        </View>
                    </TouchableOpacity>

                    <View style={styles.viewTouchSubBottom}>

                        <View style={styles.viewSubBottom}>
                            <Icon
                                name="people"
                                type="MaterialIcons"
                                color="#9ea0a4"
                            />
                            <Text style={styles.textSubBottomProperty}>
                                {numberOfMaximumGuest} people
                            </Text>
                        </View>
                        
                        <View style={styles.viewSubBottom}>
                            <Icon
                                name="bed"
                                type="font-awesome"
                                color="#9ea0a4"
                            />
                            <Text style={styles.textSubBottomProperty}>
                                {bedroomCount} bed
                            </Text>
                        </View>

                        {/* <View style={styles.viewSubBottom}>
                            <Icon
                                name="stop-circle-o"
                                type="font-awesome"
                                color="#d4841c"
                            />
                            <Text style={styles.textSubBottomProperty}>
                                stop
                            </Text>
                        </View> */}

                    </View>

                </View>

               
            </View>

        )
    }
}

const styles = StyleSheet.create({

    viewItem: {
        // flex: 1,
        flexDirection: 'row',
        borderRadius: 10,
        marginHorizontal: 2.5,
        paddingLeft: 2.5,
        marginVertical: 2.5,
        borderColor: '#F2F2F2',
        backgroundColor: '#ffff',

        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.2 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 2,
    },
    touchItem: {

    },
    viewTouchSub: {
        paddingVertical: 5,
        flex: 1,
    },

    viewTouchSubTop: {
        flexDirection: 'row',
        // maxWidth: '95%',
        // backgroundColor: 'red'
    },
    viewSubTopInfo: {
        paddingLeft: 10,
        // paddingRight: 40
    },

    imgIconItem: {
        width: 50,
        height: 50,
        borderRadius: 5,
    },
    viewTouchSubBottom: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewSubBottom: {
        flexDirection: 'row'
    },
    textSubBottomProperty: {

    },

    viewOptionManipul: {
        // flexDirection: 'row',
        justifyContent: 'space-around'
    }
});