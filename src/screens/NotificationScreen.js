import React, { Component } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import CardNotification from "../components/CardNotification";
import NotificationService from "../services/notification.service";
import LoaderModal from "../components/LoaderModal";

export default class NotificationScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listNotification: [],
            loading: false
        };
    }

    async componentDidMount() {
        this.setState({ loading: true });
        this.fetchNotification().then(()=>{
            this.setState({ loading: false });
        })
    }

    fetchNotification = async () => {

        await NotificationService.getAll()
            .then(response => {
                this.setState({
                    listNotification: response.data.data
                })
            }).catch(error => {
                alert('Error: ' + error);
            });
    }

    readNotification = async (id) => {
        await NotificationService.read(id)
            .then((response) => {
                this.fetchNotification()
            }).catch(error => {
                alert('Error: ' + error)
            })
    }

    renderCardBookingRoom = (notifications) => {
        return (
            <FlatList
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={notifications}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardNotification
                            onPress={() => this.readNotification(item.id)}
                            title={item.title}
                            content={item.content}
                            time={item.createdAt}
                            status={item.read}
                        />
                }
            />
        )
    }

    render() {
        return (
            <View style={styles.mainView}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* main area notification */}
                <View>
                    {this.renderCardBookingRoom(this.state.listNotification)}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
});