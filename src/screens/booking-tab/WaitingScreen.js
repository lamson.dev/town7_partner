import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Icon } from "react-native-elements";

export default class WaitingScreen extends Component {
    render() {
        const { hostName, address, imgUri, price, starVoting, startDate, endDate, numOfGuest, onPress } = this.props;
        return (
            <View>
                {/* tbrip card */}
                <TouchableOpacity style={styles.touchCardTrip}>
                    <View style={styles.viewTripBooked}>


                        {/* body */}
                        <View style={styles.viewBodyTripCard}>
                            <Image style={styles.imageRoom} width={35} height={35} source={ require('../../assets/images/icons/user-ic-2.png') } />
                            {/* info */}
                            <View style={styles.viewBodyInfo}>
                                {/* short info */}

                                <View style={{flexDirection: 'row'}}>
                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 150, color: 'red' }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        Booking request 14:34:33
                                    </Text>
                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 150, textAlign: 'right'}]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        02/03/2020
                                    </Text>
                                </View>

                                <Text style={[styles.textBlackColor, styles.textHostTitle, { width: 150 }]}
                                    numberOfLines={2} ellipsizeMode='tail'>
                                    Nguyen Van Guest 1
                                    </Text>

                                <View style={styles.viewAddress}>
                                    <Icon
                                        name='date-range'
                                        type='MaterialIcons'
                                        color="#1EA896"
                                        size={14} />
                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 180 }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        03/03/2020 - 02/04/2020
                                    </Text>
                                  
                                </View>
                                
                                <View style={styles.viewAddress}>
                                    <Text style={[styles.textSecondColor, styles.textSmall, { width: 200 }]}
                                        numberOfLines={1} ellipsizeMode='tail' >
                                        Prop ID: 23342 - Phong Rieng Tai Nha Nhe
                                    </Text>
                                  
                                </View>

                            </View>

                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    touchCardTrip: {
        marginTop: 5,
        marginBottom: 5 / 2,
        // marginHorizontal: 10,
    },
    imageRoom: {
        borderRadius: 3.5
      },
    viewTripBooked: {
        paddingBottom: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#c9c9c9',
        // borderRadius: 5,
        // shadowColor: "rgba(0,0,0,0.75)",
        // shadowOffset: {
        //     width: 0,
        //     height: 5
        // },
        // shadowOpacity: 0.29,
        // shadowRadius: 3.84,
        // elevation: 7,
        backgroundColor: '#fff',
        width: '100%'
    },
    viewHeadTripBooked: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 10,
    },
    textSecondColor: {
        color: '#535355',
    },
    textBlackColor: {
        color: '#302f33',
    },
    viewBodyTripCard: {
        flexDirection: 'row',
        paddingLeft: 5,
        paddingVertical: 5
    },
    viewBodyInfo: {
        // flexDirection: 'row',
        paddingHorizontal: 5,
        justifyContent: 'space-between',
        flex: 1,
    },
    viewAddress: {
        flexDirection: 'row',
        marginVertical: 5,
    },
    textHostTitle: {
        fontSize: 15,
    },
    textSmall: {
        fontSize: 11,
    },
    textStar: {
        fontSize: 13.5,
        color: '#1EA896',
    },
    viewPrice: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    textPrice: {
        fontWeight: "700"
    }
});