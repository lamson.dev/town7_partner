import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Navigation } from "react-native-navigation";
import LoaderModal from "../components/LoaderModal";
import BookingRoomService from "../services/bookingRoom.service";
export default class BookingDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookingId: props.id,
            bookingData: {},
            loading: false
        }
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await BookingRoomService.getById(this.state.bookingId).then((response) => {
            this.setState({ loading: false });
            console.log('booking data: ', response);
            this.setState({ bookingData: response.data.data });
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error: can not load your booking data: ', error);
        });

        this.setState({ loading: false });
    }

    confirmBooking = async () => {
        this.setState({ loading: true });

        await BookingRoomService.confirmBooking(this.state.bookingId).then(response => {
            this.setState({ loading: false });
            alert('You have confirmed this booking successful');
            Navigation.pop(this.props.componentId);
        }).catch(error => {
            alert('Error: can not confirm booking ' + error)
        });
        this.setState({ loading: false });
    }
    cancelBooking = async () => {
        this.setState({ loading: true });

        await BookingRoomService.cancelBooking(this.state.bookingId).then(response => {
            this.setState({ loading: false });
            alert('You have cancelled this booking successful');
            Navigation.pop(this.props.componentId);
        }).catch(error => {
            alert('Error: can not cancel booking ' + error)
        });
        this.setState({ loading: false });
    }

    render() {
        const { startDate, endDate, guests, nights, pricePerNight, serviceCharge, totalPrice, bookingStatus } = this.props;

        return (
            <View style={styles.viewMain}>
                {/* tbrip card */}
                <LoaderModal
                    loading={this.state.loading} />
                <ScrollView>

                    <View style={styles.viewCheckInOutRoom}>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Check-In</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.checkInDate}
                            </Text>
                        </View>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Check-Out</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.checkOutDate}
                            </Text>
                        </View>

                    </View>

                    <View style={styles.viewCheckInOutRoom}>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Guests</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.numberOfAdultGuest + this.state.bookingData.numberOfChildrenGuest + this.state.bookingData.numberOfInfantGuest}
                            </Text>
                        </View>

                        <View style={styles.viewItemInfo}>
                            <Text style={styles.textTouchLabel}>Staying</Text>
                            <Text style={styles.textTouchProperty}>
                                {this.state.bookingData.nights}
                            </Text>
                        </View>

                    </View>

                    <View style={styles.viewFee}>
                        <Text style={styles.textFeeTax}>
                            PAYMENT DETAIL
                        </Text>

                        <View style={styles.viewFeeTax}>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    Room price
                                </Text>
                                <Text>
                                    VND {this.state.bookingData.pricePerNight}
                                </Text>
                            </View>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    One room x {this.state.bookingData.nights} nights
                                </Text>
                                <Text>
                                    VND {(this.state.bookingData.nights * this.state.bookingData.pricePerNight)}
                                </Text>
                            </View>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    Cleaning fee
                                </Text>
                                <Text>
                                    VND {this.state.bookingData.cleanCosts}
                                </Text>
                            </View>

                            <View style={styles.viewFeeTaxSub}>
                                <Text>
                                    Service fee
                                </Text>
                                <Text>
                                    VND {this.state.bookingData.serviceCharge}
                                </Text>
                            </View>

                        </View>
                    </View>


                </ScrollView>

                <View style={styles.viewBottom}>

                    {/* Total fee */}
                    <View style={styles.viewTotalFee}>
                        <Text style={styles.textTotalFee}>
                            Total
                        </Text>
                        <Text style={[styles.textTotalFee, { fontWeight: 'bold', color: 'red' }]}>
                            VND {this.state.bookingData.totalPrice}
                        </Text>
                    </View>

                    {!this.state.bookingData.acceptedFromHost ?
                        <View style={styles.viewButtonBook}>


                            <View style={styles.viewActionBtn}>
                                <TouchableOpacity style={[styles.touchAction, { backgroundColor: '#ff6021' }]}
                                    onPress={this.cancelBooking}>
                                    <Text style={styles.textActionTouch}>
                                        CANCEL
                                </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.viewActionBtn}>
                                <TouchableOpacity style={[styles.touchAction, { backgroundColor: '#78de40' }]}
                                    onPress={this.confirmBooking}>
                                    <Text style={styles.textActionTouch}>
                                        CONFIRM
                                </Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                        : null
                    }
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        backgroundColor: '#fff'
    },

    viewRoomInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#fff'
    },
    textRoomType: {
        fontSize: 14.5,
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: '#c4194a',
        letterSpacing: 2,
        textTransform: 'uppercase'
    },
    textPrice: {
        fontSize: 20,
        fontStyle: 'normal',
        lineHeight: 24,
        color: '#282a2e',
        marginTop: 10
    },
    imageRoom: {
        width: 110,
        height: 80,
    },

    viewCheckInOutRoom: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: 20,
        marginBottom: 15,
    },
    viewItemInfo: {
        // backgroundColor: 'red',
        flex: 1
    },
    touchProperty: {
        marginBottom: 5,
        width: 100,
        height: 60,
        justifyContent: 'center',
        borderWidth: 0.7,
        borderRadius: 8,
        borderColor: '#fff',
        backgroundColor: '#FFFF',
    },
    textTouchLabel: {
        fontWeight: 'bold',
        fontSize: 10.5,
        textAlign: "left",
        textTransform: 'uppercase',
        color: '#91929e',
        marginBottom: 2
    },
    textTouchProperty: {
        textAlign: 'left',
        fontWeight: '200',
        fontSize: 17,
        lineHeight: 20,
        color: '#1e1e1f',
    },

    viewFee: {
        // backgroundColor: 'blue',
        marginTop: 10,
        marginHorizontal: 20,

        paddingTop: 15,
        borderTopWidth: 1,
        borderTopColor: '#fff',

        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#fff',
        bottom: 0,
        // backgroundColor: 'blue',

    },
    textFeeTax: {
        fontWeight: 'bold',
        color: '#1e1e1f',
        fontSize: 13
    },
    viewFeeTax: {
        marginVertical: 5,
        position: 'relative',
        // top: 10,
    },
    viewFeeTaxSub: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 5
    },

    viewTotalFee: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginVertical: 5
    },
    textTotalFee: {
        fontSize: 18.5,
    },

    viewBottom: {
        paddingBottom: 7,
    },

    viewButtonBook: {
        paddingBottom: 7,
        marginHorizontal: 19,
        justifyContent: 'space-between',
        flexDirection: 'row',
        // backgroundColor: 'blue'
    },

    viewActionBtn: {
        height: 56,
        width: 120
    },
    touchAction: {
        flex: 1,
        backgroundColor: '#24c3f0',
        justifyContent: 'center',
        borderRadius: 12.5,
        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.5 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 5,
    },
    textActionTouch: {
        color: '#FFFFFF',
        fontSize: 21.2,
        fontWeight: "normal",
        fontStyle: 'normal',
        textAlign: 'center'
    },
});