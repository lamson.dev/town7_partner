import { REGISTER_USER_REQUEST, AUTH_USER_REQUEST, LOGOUT_REQUEST } from "./types";

export const registerUserAction = (user) => {
    console.log('-lg register action');
    return {
        type: REGISTER_USER_REQUEST,
        user
    }
}

export const loginAction = (user) => {
    console.log('-lg action: ', user);
    return {
        type: AUTH_USER_REQUEST,
        user
    }
}

export const logoutAction = () => {
    console.log('-lg logout action');
    return {
        type: LOGOUT_REQUEST,
    }
}