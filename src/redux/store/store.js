import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import rootReducer from "../reducers/index";
import rootSaga from "../sagas/index";

const sagaMiddleware = createSagaMiddleware();

// const store = () => {
//     // const sagaMiddleware = createSagaMiddleware();

//     const configureStore = createStore(
//         rootReducer,
//         // applyMiddleware(sagaMiddleware)
//     );
//     // sagaMiddleware.run(rootSaga);
//     return configureStore;

// }

const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(rootSaga);

export default store;