import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";

export async function fetchUserDataService() {
    return fetchApi("/users/me", "GET", null, await deviceStorage.loadToken());
}
