import { fetchApi } from "./api/api";
import { getToken } from "./auth-header.service";

class NotificationService {

    async getAll() {
        return await fetchApi("/notifications/users", "GET", null, await getToken());
    }

    async read(id) {
        return await fetchApi("/notifications/users/"+id, "PUT", null, await getToken());
    }
    
}

export default new NotificationService();
